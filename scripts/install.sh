#!/usr/bin/env bash

# Install codebase dependencies.
cd /var/www/html
composer install --no-dev
npm install
./node_modules/.bin/gulp --gulpfile ./gulpfile.js