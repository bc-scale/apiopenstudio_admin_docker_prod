ARG UBUNTU_VERSION=22.04

FROM ubuntu:${UBUNTU_VERSION}

LABEL org.opencontainers.image.authors="https://www.apiopenstudio.com" \
    org.opencontainers.image.description="ApiOpenStdio Admin application packaged by ApiOpenStdio" \
    org.opencontainers.image.source="https://gitlab.com/apiopenstudio/docker_images/apiopenstudio_admin_docker_prod" \
    org.opencontainers.image.title="ApiOpenStudio Admin Docker Prod" \
    org.opencontainers.image.vendor="Naala Pty Ltd" \
    org.opencontainers.image.version="1.0.0"

ARG PHP_VERSION=8.1
ARG COMPOSER_VERSION=2.4.4
ARG NODE_VERSION=13.14.0
ARG REPOSITORY=https://github.com/naala89/apiopenstudio_admin
ARG BRANCH
ARG TAG

ENV NVM_DIR /root/.nvm
ENV DEBIAN_FRONTEND noninteractive
ENV PHP_VERSION=${PHP_VERSION}
ENV NODE_VERSION=${NODE_VERSION}
ENV COMPOSER_VERSION=${COMPOSER_VERSION}
ENV REPOSITORY=${REPOSITORY}
ENV BRANCH=${BRANCH}
ENV TAG=${TAG}

RUN apt-get update \
    && apt-get install -yq \
    curl \
    g++ \
    git \
    make \
    nginx \
    openssh-client \
    php${PHP_VERSION}-fpm \
    php${PHP_VERSION}-cli \
    php${PHP_VERSION}-curl \
    python2 \
    unzip \
    zip

# Install Composer
RUN curl -o /tmp/composer-setup.php https://getcomposer.org/installer \
    && curl -o /tmp/composer-setup.sig https://composer.github.io/installer.sig \
    && php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }" \
    && php /tmp/composer-setup.php --no-ansi --install-dir=/usr/local/bin --filename=composer --version=${COMPOSER_VERSION}

# Install node and npm
RUN mkdir $NVM_DIR
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.39.2/install.sh | bash
RUN chmod +x $NVM_DIR/nvm.sh
RUN . $NVM_DIR/nvm.sh && nvm install $NODE_VERSION && nvm alias default $NODE_VERSION && nvm use default
RUN ln -sf $NVM_DIR/versions/node/v$NODE_VERSION/bin/node /usr/bin/nodejs
RUN ln -sf $NVM_DIR/versions/node/v$NODE_VERSION/bin/node /usr/bin/node
RUN ln -sf $NVM_DIR/versions/node/v$NODE_VERSION/bin/npm /usr/bin/npm

# Add application codebase
WORKDIR /tmp
COPY scripts/download.sh .
RUN chmod +x download.sh
RUN ["/bin/bash", "-c", "./download.sh ${REPOSITORY} ${BRANCH} ${TAG}"]
RUN rm ./download.sh

# Nginx config
COPY config/nginx/nginx.conf /etc/nginx/nginx.conf
COPY config/nginx/sites-available/default /etc/nginx/sites-available/default
RUN mkdir -p /etc/nginx/certs/

WORKDIR /var/www/html
COPY scripts/install.sh /var/www/html/
RUN chmod 754 /var/www/html/install.sh

EXPOSE 80
EXPOSE 443

CMD ["/bin/bash", "-c", "/usr/sbin/service php8.1-fpm start && nginx -g 'daemon off;'"]
