# naala89/apiopenstudio_admin

This creates and uploads a docker image for the production use of ApiOpenStudio Admin.

It contains:

* Nginx
* PHP 8.1
* The tagged version of ApiOpenStudio Admin
* composer
* node

# Getting Started

This container contains a full working Nginx/PHP server and ApiOpenStudio
Admin. It requires a separate instance of ApiOpenStudio Core to connect with,
and can be quickly installed on a bare-bones production server in minutes to
have a fully functioning ApiOpenStudio Admin instance.

**Note:** There should not be a running instance of Apache or Nginx on the
server, otherwise the incoming port 80/443 requests will not get directed to
the Docker container.

# Prerequisites

In order to run this container you'll need docker installed.

* [Windows][docker_windows]
* [OS X][docker_osx]
* [Linux][docker_linux]

You will also need a running instance of ApiOpenStudio core installed on a
SEPARATE server.

* [ApiOpenStudio Docker][docker_apiopenstudio]

# Usage

## Useful File Locations

* `/etc/nginx/nginx.conf` - The main Nginx config file.
* `/etc/nginx/sites-enabled/default` - The Nginx site config file.
* `/etc/php/8.1/{cli,fpm}/php.ini` - PHP configuration.
* `/var/www/html/install.sh` - Install composer, npm and gulp dependencies.

## Example

```shell
# Download and configure the settings file.
sudo mkdir -p /data/apiopenstudio_admin
sudo curl https://raw.githubusercontent.com/naala89/apiopenstudio_admin/master/example.settings.yml --output /data/apiopenstudio_admin/settings.yml

# Update the settings.yml file

# Start the ApiOpenStudio Admin container
sudo docker run -d --name apiopenstudio-admin \
-p 80:80 \
-p 443:443 \
--mount type=bind,source=/data/apiopenstudio_admin/settings.yml,target=/var/www/html/settings.yml \
--mount type=bind,source=/path/to/ssl.crt,target=/etc/nginx/certs/apiopenstudio_admin.crt \
--mount type=bind,source=/path/to/ssl.key,target=/etc/nginx/certs/apiopenstudio_admin.key \
naala89/apiopenstudio_admin:<tag>

# SSH into the container
docker exec -it apiopenstudio-admin bash

# Install the dependencies.
./install.sh
```

## Detailed instructions

Visit the [ApiOpenStudio wiki][wiki] for full instructions and
downloading/configuration of the `settings.yml` file.

# Find Us

* [www.apiopenstudio.com][web_apiopenstudio]
* [Wiki][wiki]
* [GitHub][github_naala89]
* [GitLab (ApiOpenStudio Core)][gitlab_apiopenstudio]
* [GitLab (ApiOpenStudio Admin)][gitlab_apiopenstudio_admin]

# Versioning

This image automatically built from tags on the master branch at
[GitLab (ApiOpenStudio Admin)][gitlab_apiopenstudio_admin].

Version tags (i.e. `naala89/apiopenstudio_admin:1.0`) references the version of the
image and the tag of the release.

Version tags of `master` & `develop` (i.e. `naala89/apiopenstudio_admin:master`)
references the latest version on the branch.

The `latest` tag is most recent built image and should not be used to pull the
latest tagged version.

# Image Developers

This will not run in pipelines - GitLab does not currently support BuildKit and
SSH forwarding. This upshot of this, is that the SSH key used to clone the
repository would be need to added to the build, and would be revealed in the
image layers. So this must be run locally.

## Setup SSH keys

To build the image locally, you need to first setup SSH keys:

``` shell
ssh-add ~/.ssh/id_rsa
ssh-add -l
```

## `.env`

```dotenv
REGISTRY=docker.io
REPOSITORY=git@gitlab.com:apiopenstudio/apiopenstudio_admin.git
IMAGE_NAME=apiopenstudio_admin
REGISTRY_USER=<docker_username>
REGISTRY_PASSWORD=<docker_password>
```

## Run build & upload command:

``` shell
# Upload an image of branch_name
./upload.sh -b branch_name

# Upload an image of tag_name
./upload.sh -t tag_name
```

## Parameters in the upload script

You must declare either `-b` or `-t` parameters, to specify what version of
ApiOpenStudio Admin to publish and tag.

All other parameters are optional and will override the `.env` file.

* `-b` Repository branch
* `-t` Repository tag
* `-r` Repository SSH clone path
* `-i` Docker Hub Image name
* `-u` Docker user name
* `-p` Docker user password
* `-h` Command help

# Authors

* **John Avery** - [John - GitLab][gitlab_john]

# License

This project is licensed under the MIT License - see the [LICENSE.md][license]
file for details.

[docker_windows]: https://docs.docker.com/desktop/install/windows-install/

[docker_osx]: https://docs.docker.com/desktop/install/mac-install/

[docker_linux]: https://docs.docker.com/desktop/install/linux-install/

[docker_hub]: https://hub.docker.com/

[docker_apiopenstudio]: https://hub.docker.com/repository/docker/naala89/apiopenstudio

[gitlab_apiopenstudio]: https://gitlab.com/apiopenstudio/apiopenstudio

[gitlab_apiopenstudio_admin]: https://gitlab.com/apiopenstudio/apiopenstudio_admin

[github_naala89]: https://github.com/naala89

[web_apiopenstudio]: https://www.apiopenstudio.com

[gitlab_john]: https://gitlab.com/john89

[license]: https://gitlab.com/apiopenstudio/docker_images/apiopenstudio_admin_docker_prod/-/blob/master/LICENSE.md

[wiki]: https://wiki.apiopenstudio.com/en/install/admin-gui/settings
