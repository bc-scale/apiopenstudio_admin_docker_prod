#!/usr/bin/env bash

# Set fonts for Help.
NORM=`tput sgr0`
BOLD=`tput bold`
REV=`tput smso`

# Set Script Name variable
SCRIPT=`basename ${BASH_SOURCE[0]}`

# Import the .env file.
set -o allexport
source .env
set +o allexport

# Help function
function HELP {
  echo -e \\n"Help documentation for ${BOLD}${SCRIPT}.${NORM}"\\n
  echo -e "${REV}Basic usage:${NORM} ${BOLD}${SCRIPT} -t tag-name${NORM}"\\n
  echo "Command line switches are optional. The following switches are recognized."
  echo "${REV}-b${NORM}  --Sets the value for option ${BOLD}BRANCH${NORM}."
  echo "${REV}-t${NORM}  --Sets the value for option ${BOLD}TAG${NORM}."
  echo "${REV}-r${NORM}  --Sets the value for option ${BOLD}REPOSITORY${NORM}."
  echo "${REV}-i${NORM}  --Sets the value for option ${BOLD}IMAGE_NAME${NORM}."
  echo "${REV}-u${NORM}  --Sets the value for option ${BOLD}REGISTRY_USER${NORM}."
  echo "${REV}-p${NORM}  --Sets the value for option ${BOLD}REGISTRY_PASSWORD${NORM}."
  echo -e "${REV}-h${NORM}  --Displays this help message. No further functions are performed."\\n
  echo -e "Example: ${BOLD}${SCRIPT} -b master${NORM}"
  echo -e "Example: ${BOLD}${SCRIPT} -t 1.0.1${NORM}"
  exit 1
}

while getopts b:t:r:i:h flag; do
  case "${flag}" in
    b)
      BRANCH=${OPTARG}
      ;;
    t)
      TAG=${OPTARG}
      ;;
    r)
      REPOSITORY=${OPTARG}
      ;;
    i)
      IMAGE_NAME=${OPTARG}
      ;;
    u)
      REGISTRY_USER=${OPTARG}
      ;;
    p)
      REGISTRY_PASSWORD=${OPTARG}
      ;;
    h)
      HELP
      ;;
    \?)
      echo -e \\n"Option -${BOLD}${OPTARG}${NORM} not allowed."
      HELP
      exit 1
      ;;
    esac
done

if [[ -z ${BRANCH} ]] && [[ -z ${TAG} ]]; then
  echo -e \\n"You must specify a branch or tag (options: ${BOLD}-b${NORM} or ${BOLD}-t${NORM})."
  exit 1
fi

echo ${REGISTRY_PASSWORD} | docker login -u ${REGISTRY_USER} --password-stdin ${REGISTRY}

if [[ ! -z "${BRANCH}" ]]; then
  IMAGE_TAG=${BRANCH}
else
  IMAGE_TAG=${TAG}
fi

docker buildx build -t "${IMAGE_NAME}" \
  --platform linux/amd64 \
  --ssh default \
  --build-arg REPOSITORY="${REPOSITORY}" \
  --build-arg BRANCH="${BRANCH}" \
  --build-arg TAG="${TAG}" .

docker tag "${IMAGE_NAME}" "${REGISTRY_USER}/${IMAGE_NAME}:${IMAGE_TAG}"

docker push "${REGISTRY_USER}/${IMAGE_NAME}:${IMAGE_TAG}"
